import unittest

def cek_string(kata1, kata2) :
	if len(kata1) != len(kata2) :
		return False
	else :
		cek = True
		i = 0
		while i < len(kata1) and (cek):
			if kata1[i] != kata2[i] and kata1[i].upper() != kata2[i] and kata1[i] != kata2[i].upper() :
				cek = False
			i += 1
		
		return cek
	
class Test(unittest.TestCase) :
	def test_panjang_beda(self) :
		self.assertFalse(cek_string("test", "tes"))
	
	def test_panjang_sama_beda_huruf(self) :
		self.assertFalse(cek_string("test", "tesd"))
	
	def test_panjang_sama_huruf_sama(self) :
		self.assertTrue(cek_string("test", "test"))
	
	def test_panjang_sama_huruf_sama_beda_kapital(self) :
		self.assertTrue(cek_string("test", "tEst"))


if __name__ == '__main__' :
	kata1 = input("masukkan string pertama :")
	kata2 = input("masukkan string kedua :")
	print("hasil cek_string :",cek_string(kata1, kata2))

