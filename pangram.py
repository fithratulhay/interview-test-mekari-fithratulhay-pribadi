import string
import unittest

def check_pangram(kalimat) :
	check = {}

	for i in string.ascii_letters :
		check[i] = False
	
	for huruf in kalimat :
		check[huruf] = True

	for i in string.ascii_lowercase :
		if (not check[i]) and (not check[i.upper()]) :
			return False

	return True  

class Test(unittest.TestCase) :
	def test_true(self) :
		self.assertTrue(check_pangram("Jived fox nymph grabs quick waltz."))
	
	def test_false(self) :
		self.assertFalse(check_pangram("pangram"))
	
if __name__ == '__main__' :
	kalimat = input("input kalimat : ")
	print("hasil check_pangram :",check_pangram(kalimat))